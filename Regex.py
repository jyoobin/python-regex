#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 12:47:36 2018

@author: yoobin
"""

import csv
import re

# group 1: (\w+)   First or last name 
# group 2: (\,)    comma (determine whether the group 1 or the group 2 is the last name)
# group 3: (\D\.)  Middle name if not None
# group 4: (\w+)   last or first name
# group 5: (\D\.)  Middle name if not None
# group 6: (\d{3}) first 3 digits of phone number
# group 7: (\d{3}) middle 3 digits of phone number
# group 8: (\d{4}) last 4 digits of phone number
regex = r'(\w+)\s*(\,)?(\D\.)?\s*(\w+)\s*(\D\.)?\s*1?-?\(?(\d{3})\)?\.?-?\s*(\d{3})\W?(\d{4})'

matches = []
with open("data.csv",'r+') as fr:
    reader = csv.reader(fr)
    for row in reader:
        find = " ".join(row)
        if re.search(regex, find):
            match = re.search(regex, find)
            matches.append(match)

with open("data2.csv", 'w+') as fw:
    writer = csv.writer(fw)
    # Header row
    writer.writerow(['First'+' '*10]+['M.I.'+' '*10]+['Last'+' '*10]+['Number'+' '*10])
    
    for row in matches: 
        if row.group(2) != None:        # if comma is present, group 1 is last name
            if row.group(3) != None:    # it has middle name
                writer.writerow([row.group(4)]+[row.group(3)]+[row.group(1)]+['(' + row.group(6)
                     + ') ' + row.group(7) + '-' + row.group(8)])
            elif row.group(5) != None:  # it has middle name
                writer.writerow([row.group(4)]+[row.group(5)]+[row.group(1)]+['(' + row.group(6)
                     + ') ' + row.group(7) + '-' + row.group(8)])
            else:                       # it has no middle name
                writer.writerow([row.group(4)]+[None]+[row.group(1)]+['(' + row.group(6)
                     + ') ' + row.group(7) + '-' + row.group(8)])
    
        else:                           # if comma is not there, group 4 is last name
            if row.group(3) != None:    # it has middle name
                writer.writerow([row.group(1)]+[row.group(3)]+[row.group(4)]+['(' + row.group(6)
                     + ') ' + row.group(7) + '-' + row.group(8)])
            elif row.group(5) != None:  # it has middle name
                writer.writerow([row.group(1)]+[row.group(5)]+[row.group(4)]+['(' + row.group(6)
                     + ') ' + row.group(7) + '-' + row.group(8)])
            else:                       # it has no middle name
                writer.writerow([row.group(1)]+[None]+[row.group(4)]+['(' + row.group(6)
                     + ') ' + row.group(7) + '-' + row.group(8)])





